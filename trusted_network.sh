echo "*************BEFORE*************"
echo "*************BEFORE*************"
echo "*************BEFORE*************"
echo ""
sudo iptables -L
sudo iptables -F
sudo iptables -P INPUT ACCEPT
sudo iptables -P OUTPUT ACCEPT
sudo iptables -P FORWARD ACCEPT

sudo ip6tables -L
sudo ip6tables -F
sudo ip6tables -P INPUT ACCEPT
sudo ip6tables -P OUTPUT ACCEPT
sudo ip6tables -P FORWARD ACCEPT
echo "*************CLEAR*************"
echo "*************CLEAR*************"
echo "*************CLEAR*************"
echo ""
sudo iptables -L
sudo ip6tables -L

sudo iptables -P INPUT DROP
sudo iptables -P OUTPUT DROP
sudo iptables -P FORWARD DROP
sudo ip6tables -P INPUT DROP
sudo ip6tables -P OUTPUT DROP
sudo ip6tables -P FORWARD DROP

sudo iptables -N BRUTEFORCE_FTP
sudo ip6tables -N BRUTEFORCE_FTP

sudo iptables -I BRUTEFORCE_FTP -p tcp --dport 21 -m state --state NEW -m recent --set
sudo iptables -I BRUTEFORCE_FTP -p tcp --dport 21 -m state --state NEW -m recent  --update --seconds 86400 --hitcount 25 -j DROP
sudo ip6tables -I BRUTEFORCE_FTP -p tcp --dport 21 -m state --state NEW -m recent --set
sudo ip6tables -I BRUTEFORCE_FTP -p tcp --dport 21 -m state --state NEW -m recent  --update --seconds 86400 --hitcount 25 -j DROP

sudo iptables -N BRUTEFORCE_SSH
sudo ip6tables -N BRUTEFORCE_SSH

sudo iptables -I BRUTEFORCE_SSH -p tcp --dport 22 -m state --state NEW -m recent --set
sudo iptables -I BRUTEFORCE_SSH -p tcp --dport 22 -m state --state NEW -m recent  --update --seconds 86400 --hitcount 25 -j DROP
sudo ip6tables -I BRUTEFORCE_SSH -p tcp --dport 22 -m state --state NEW -m recent --set
sudo ip6tables -I BRUTEFORCE_SSH -p tcp --dport 22 -m state --state NEW -m recent  --update --seconds 86400 --hitcount 25 -j DROP

sudo iptables -N INPUT_TCP_PORTS
sudo ip6tables -N INPUT_TCP_PORTS

sudo iptables -I INPUT_TCP_PORTS -p TCP -m multiport --dport 80,22,21,443,445,3389,8000,5000,8080 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I INPUT_TCP_PORTS -p TCP -m multiport --dport 3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -I INPUT_TCP_PORTS -p TCP -m multiport --dport 80,22,21,443,8000,5000,8080 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -I INPUT_TCP_PORTS -p TCP -m multiport --dport 3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268 --syn -m conntrack --ctstate NEW -j ACCEPT

sudo iptables -N INPUT_TCP
sudo ip6tables -N INPUT_TCP

sudo iptables -I INPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo iptables -I INPUT_TCP -p tcp ! --syn -m conntrack --ctstate NEW -j DROP
sudo iptables -I INPUT_TCP -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP
sudo iptables -I INPUT_TCP -p tcp -j INPUT_TCP_PORTS
sudo iptables -I INPUT_TCP -m state --state NEW -m recent --set
sudo iptables -I INPUT_TCP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP
sudo iptables -I INPUT_TCP -p tcp --dport 22 -j BRUTEFORCE_SSH
sudo iptables -I INPUT_TCP -p tcp --dport 21 -j BRUTEFORCE_FTP
sudo ip6tables -I INPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo ip6tables -I INPUT_TCP -p tcp ! --syn -m conntrack --ctstate NEW -j DROP
sudo ip6tables -I INPUT_TCP -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP
sudo ip6tables -I INPUT_TCP -p tcp -j INPUT_TCP_PORTS
sudo ip6tables -I INPUT_TCP -m state --state NEW -m recent --set
sudo ip6tables -I INPUT_TCP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP
sudo ip6tables -I INPUT_TCP -p tcp --dport 22 -j BRUTEFORCE_SSH
sudo ip6tables -I INPUT_TCP -p tcp --dport 21 -j BRUTEFORCE_FTP

sudo iptables -N INPUT_UDP_PORTS
sudo ip6tables -N INPUT_UDP_PORTS

sudo iptables -I INPUT_UDP_PORTS -p udp -m multiport --sport 68,53,1900,67,5355,5353,137 -j ACCEPT
sudo ip6tables -I INPUT_UDP_PORTS -p udp -m multiport --sport 68,53,1900,67,5355,5353,137 -j ACCEPT

sudo iptables -N INPUT_UDP
sudo ip6tables -N INPUT_UDP

sudo iptables -I INPUT_UDP -p udp -j INPUT_UDP_PORTS
sudo iptables -I INPUT_UDP -m state --state NEW -m recent --set
sudo iptables -I INPUT_UDP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP
sudo ip6tables -I INPUT_UDP -p udp -j INPUT_UDP_PORTS
sudo ip6tables -I INPUT_UDP -m state --state NEW -m recent --set
sudo ip6tables -I INPUT_UDP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP

sudo iptables -N INPUT_ICMP
sudo ip6tables -N INPUT_ICMP

sudo iptables -I INPUT_ICMP -p icmp --icmp-type echo-request -m limit --limit 1/second -j ACCEPT
sudo iptables -I INPUT_ICMP -p icmp --icmp-type 11 -m limit --limit 1/second -j ACCEPT
sudo iptables -I INPUT_ICMP -p ICMP --icmp-type echo-reply -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p icmpv6 --icmpv6-type echo-request -m limit --limit 1/second -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p icmpv6 --icmpv6-type 11 -m limit --limit 1/second -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p icmpv6 --icmpv6-type echo-reply -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-advertisement -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-solicitation -j ACCEPT

sudo iptables -N INPUT_PROTO
sudo iptables -I INPUT_PROTO -p tcp -j INPUT_TCP
sudo iptables -I INPUT_PROTO -p udp -j INPUT_UDP
sudo iptables -I INPUT_PROTO -p icmp -j INPUT_ICMP
sudo ip6tables -N INPUT_PROTO
sudo ip6tables -I INPUT_PROTO -p tcp -j INPUT_TCP
sudo ip6tables -I INPUT_PROTO -p udp -j INPUT_UDP
sudo ip6tables -I INPUT_PROTO -p icmpv6 -j INPUT_ICMP

sudo iptables -N OUTPUT_ICMP
sudo iptables -I OUTPUT_ICMP -p ICMP --icmp-type echo-reply -m limit --limit 1/second -j ACCEPT
sudo iptables -I OUTPUT_ICMP -p ICMP --icmp-type echo-request -j ACCEPT
sudo ip6tables -N OUTPUT_ICMP
sudo ip6tables -I OUTPUT_ICMP -p icmpv6 --icmpv6-type echo-reply -m limit --limit 1/second -j ACCEPT
sudo ip6tables -I OUTPUT_ICMP -p icmpv6 --icmpv6-type echo-request -j ACCEPT
sudo ip6tables -I OUTPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-advertisement -j ACCEPT
sudo ip6tables -I OUTPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-solicitation -j ACCEPT

sudo iptables -N OUTPUT_TCP_PORTS
sudo ip6tables -N OUTPUT_TCP_PORTS

sudo iptables -I OUTPUT_TCP_PORTS -p TCP -m multiport --dport 80,443,22,21,25,587,465,143,993,110,995,389,636 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I OUTPUT_TCP_PORTS -p TCP -m multiport --dport 445,3389,8001,5001,5000,8080,8088,8000 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I OUTPUT_TCP_PORTS -p TCP -m multiport --dport 3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -I OUTPUT_TCP_PORTS -p TCP -m multiport --dport 80,443,22,21,25,587,465,143,993,110,995,389,636 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -I OUTPUT_TCP_PORTS -p TCP -m multiport --dport 8001,5001,5000,8080,8088,8000 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -I OUTPUT_TCP_PORTS -p TCP -m multiport --dport 3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268 --syn -m conntrack --ctstate NEW -j ACCEPT

sudo iptables -N OUTPUT_TCP
sudo iptables -I OUTPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo iptables -I OUTPUT_TCP -p TCP -j OUTPUT_TCP_PORTS
sudo ip6tables -N OUTPUT_TCP
sudo ip6tables -I OUTPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo ip6tables -I OUTPUT_TCP -p TCP -j OUTPUT_TCP_PORTS

sudo iptables -N OUTPUT_UDP_PORTS
sudo ip6tables -N OUTPUT_UDP_PORTS
sudo iptables -I OUTPUT_UDP_PORTS -p udp -m multiport --dport 68,53,1900,67,5355,5353,137 -j ACCEPT
sudo ip6tables -I OUTPUT_UDP_PORTS -p udp -m multiport --dport 68,53,1900,67,5355,5353,137 -j ACCEPT

sudo iptables -N OUTPUT_UDP
sudo iptables -I OUTPUT_UDP -p udp -j OUTPUT_UDP_PORTS
sudo ip6tables -N OUTPUT_UDP
sudo ip6tables -I OUTPUT_UDP -p udp -j OUTPUT_UDP_PORTS

sudo iptables -N OUTPUT_PROTO
sudo iptables -I OUTPUT_PROTO -p tcp -j OUTPUT_TCP
sudo iptables -I OUTPUT_PROTO -p udp -j OUTPUT_UDP
sudo iptables -I OUTPUT_PROTO -p icmp -j OUTPUT_ICMP
sudo ip6tables -N OUTPUT_PROTO
sudo ip6tables -I OUTPUT_PROTO -p tcp -j OUTPUT_TCP
sudo ip6tables -I OUTPUT_PROTO -p udp -j OUTPUT_UDP
sudo ip6tables -I OUTPUT_PROTO -p icmpv6 -j OUTPUT_ICMP

sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A OUTPUT -o lo -j ACCEPT
sudo ip6tables -A INPUT -i lo -j ACCEPT
sudo ip6tables -A OUTPUT -o lo -j ACCEPT

sudo iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
sudo iptables -A INPUT -j INPUT_PROTO
sudo iptables -A OUTPUT -j OUTPUT_PROTO
sudo ip6tables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
sudo ip6tables -A INPUT -j INPUT_PROTO
sudo ip6tables -A OUTPUT -j OUTPUT_PROTO

sudo sysctl -w net.ipv4.conf.all.route_localnet=1
sudo iptables -t nat -I PREROUTING -p tcp --dport 8001 -s 192.168.0.0/16 -j DNAT --to 127.0.0.1:8000
sudo iptables -t nat -I PREROUTING -p tcp --dport 5001 -s 192.168.0.0/16 -j DNAT --to 127.0.0.1:5000
sudo iptables -t nat -I PREROUTING -p tcp --dport 8081 -s 192.168.0.0/16 -j DNAT --to 127.0.0.1:8080
sudo ip6tables -t nat -I PREROUTING -p tcp --dport 8001 -s fe80::/10 -j DNAT --to [::1]:8000
sudo ip6tables -t nat -I PREROUTING -p tcp --dport 5001 -s fe80::/10 -j DNAT --to [::1]:5000
sudo ip6tables -t nat -I PREROUTING -p tcp --dport 8081 -s fe80::/10 -j DNAT --to [::1]:8080

echo "*************AFTER*************"
echo "*************AFTER*************"
echo "*************AFTER*************"
echo ""
sudo iptables -L
sudo ip6tables -L