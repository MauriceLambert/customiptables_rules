# Firewall

## IPv4

### FLUSH ALL

```bash
sudo iptables -F
```

### DROP ALL

```bash
sudo iptables -P INPUT DROP
sudo iptables -P OUTPUT DROP
sudo iptables -P FORWARD DROP
```

### ACCEPT localhost

```bash
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A OUTPUT -o lo -j ACCEPT
```

### Create INPUT chains

#### Create TCP INPUT chains

```bash
sudo iptables -N INPUT_TCP
```

##### ACCEPT ESTABLISHED connections

```bash
sudo iptables -I INPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
```

##### DROP to large packets and NEW connection without SYN

```bash
sudo iptables -I INPUT_TCP -p tcp ! --syn -m conntrack --ctstate NEW -j DROP
sudo iptables -I INPUT_TCP -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP
```

##### DROP SSH bruteforce

```bash
sudo iptables -N BRUTEFORCE_SSH
sudo iptables -I BRUTEFORCE_SSH -p tcp --dport 22 -m state --state NEW -m recent --set
sudo iptables -I BRUTEFORCE_SSH -p tcp --dport 22 -m state --state NEW -m recent  --update --seconds 86400 --hitcount 25 -j DROP
```

##### DROP FTP bruteforce

```bash
sudo iptables -N BRUTEFORCE_FTP
sudo iptables -I BRUTEFORCE_FTP -p tcp --dport 21 -m state --state NEW -m recent --set
sudo iptables -I BRUTEFORCE_FTP -p tcp --dport 21 -m state --state NEW -m recent  --update --seconds 86400 --hitcount 25 -j DROP
```
##### ACCEPT NEW connection by port

 Make a new IPTABLES chain for authorised ports and call it. If you need to add a port you can keep bruteforce or port scanning protection because this chain is protected.

 - HTTP(S) server
 - SSH server
 - FTP server
 - RDP server
 - SMB server
 - Custom HTTP server
 - Databases server (MySQL, ORACLE, MongoDB, PostgreSQL, SQLServer)

```bash
sudo iptables -N INPUT_TCP_PORTS

sudo iptables -I INPUT_TCP_PORTS -p TCP -m multiport --dport 80,22,21,443,445,3389,8000,5000,8080 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I INPUT_TCP_PORTS -p TCP -m multiport --dport 3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268 --syn -m conntrack --ctstate NEW -j ACCEPT

sudo iptables -I INPUT_TCP -p tcp -j INPUT_TCP_PORTS
```

##### DROP Port Scan

```bash
sudo iptables -I INPUT_TCP -m state --state NEW -m recent --set
sudo iptables -I INPUT_TCP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP
```

##### Call special proto rules (SSH and FTP)

```bash
sudo iptables -I INPUT_TCP -p tcp --dport 22 -j BRUTEFORCE_SSH
sudo iptables -I INPUT_TCP -p tcp --dport 21 -j BRUTEFORCE_FTP
```

#### Create UDP INPUT chains

```bash
sudo iptables -N INPUT_UDP
```

##### ACCEPT connection by port (example with ports 68,53)

 - ACCEPT DNS (Name Resolution)
 - ACCEPT DHCP (get IP address)
 - ACCEPT NetBIOS (Local Name Resolution)
 - ACCEPT LLMNR (Local Name Resolution)
 - ACCEPT MDNS (Local Name Resolution)
 - SSDP

```bash
sudo iptables -N INPUT_UDP_PORTS
sudo iptables -I INPUT_UDP_PORTS -p udp -m multiport --dport 68,53,1900,67,5355,5353,137 -j ACCEPT
sudo ip6tables -I INPUT_UDP -p udp -j INPUT_UDP_PORTS
```

##### DROP Port scan and UDP flooding

```bash
sudo iptables -I INPUT_UDP -m state --state NEW -m recent --set
sudo iptables -I INPUT_UDP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP
```

#### Create ICMP INPUT chains

```bash
sudo iptables -N INPUT_ICMP
```

##### ACCEPT ping and Timeout Error and limit ICMP flooding

```bash
sudo iptables -I INPUT_ICMP -p icmp --icmp-type echo-request -m limit --limit 1/second -j ACCEPT
sudo iptables -I INPUT_ICMP -p icmp --icmp-type 11 -m limit --limit 1/second -j ACCEPT
```

##### ACCEPT ping response

```bash
sudo iptables -I INPUT_ICMP INPUT -p ICMP --icmp-type echo-reply -j ACCEPT
```

#### Call INPUT chains

```bash
sudo iptables -N INPUT_PROTO
sudo iptables -I INPUT_PROTO -p tcp -j INPUT_TCP
sudo iptables -I INPUT_PROTO -p udp -j INPUT_UDP
sudo iptables -I INPUT_PROTO -p icmp -j INPUT_ICMP
```

### Create OUTPUT chains

#### Create ICMP OUTPUT chain

```bash
sudo iptables -N OUTPUT_ICMP
```

##### Limit ping reply

```bash 
sudo iptables -I OUTPUT_ICMP -p ICMP --icmp-type echo-reply -m limit --limit 1/second -j ACCEPT
```

##### ACCEPT ping request

```bash
sudo iptables -I OUTPUT_ICMP -p ICMP --icmp-type echo-request -j ACCEPT
```

#### Create TCP OUTPUT chain

```bash
sudo iptables -N OUTPUT_TCP
```

##### ACCEPT TCP OUTPUT ESTABLISHED connections

```bash
sudo iptables -I OUTPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
```

##### ACCEPT TCP OUTPUT NEW connections by ports

 - HTTP(S) client (Web browser)
 - SMTP(S) client (Send mail)
 - POP3(S) client (Receive mail)
 - IMAP(S) client (Receive mail)
 - RDP client
 - SMB client
 - Custom HTTP services
 - Databases client (MySQL, ORACLE, MongoDB, PostgreSQL, SQLServer)
 - LDAP(S) (Authentication)

```bash
sudo iptables -N OUTPUT_TCP_PORTS
sudo iptables -I OUTPUT_TCP_PORTS -p TCP -m multiport --dport 80,443,22,21,25,587,465,143,993,110,995,389,636 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I OUTPUT_TCP_PORTS -p TCP -m multiport --dport 445,3389,8001,5001,5000,8080,8088,8000 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I OUTPUT_TCP_PORTS -p TCP -m multiport --dport 3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I OUTPUT_TCP -p TCP -j OUTPUT_TCP_PORTS
```

#### Create UDP OUTPUT chain

```bash
sudo iptables -N OUTPUT_UDP
```

##### ACCEPT UDP OUTPUT by port

 - ACCEPT DNS (Name Resolution)
 - ACCEPT DHCP (get IP address)
 - ACCEPT NetBIOS (Local Name Resolution)
 - ACCEPT LLMNR (Local Name Resolution)
 - ACCEPT MDNS (Local Name Resolution)
 - SSDP

```bash
sudo iptables -N OUTPUT_UDP_PORTS
sudo iptables -I OUTPUT_UDP_PORTS -p udp -m multiport --dport 68,53,1900,67,5355,5353,137 -j ACCEPT
sudo iptables -I OUTPUT_UDP -p udp -j OUTPUT_UDP_PORTS
```

#### Call OUTPUT chain

```bash
sudo iptables -N OUTPUT_PROTO
sudo iptables -I OUTPUT_PROTO -p tcp -j OUTPUT_TCP
sudo iptables -I OUTPUT_PROTO -p udp -j OUTPUT_UDP
sudo iptables -I OUTPUT_PROTO -p icmp -j OUTPUT_ICMP
```

### Call chains and DROP INVALD packets

```bash
sudo iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
sudo iptables -A INPUT -j INPUT_PROTO
sudo iptables -A OUTPUT -j OUTPUT_PROTO
```

### NAT with port redirect for custom HTTP service

Some custom HTTP services have weakness, to protect this services against port scanning i redirect some ports.
 - Launch your custom service on 127.0.0.1:8000 and use it on 192.168.0.0/16:8001
 - Launch your custom service on 127.0.0.1:5000 and use it on 192.168.0.0/16:5001
 - Launch your custom service on 127.0.0.1:8080 and use it on 192.168.0.0/16:8081

```bash
sudo sysctl -w net.ipv4.conf.all.route_localnet=1
sudo iptables -t nat -I PREROUTING -p tcp --dport 8001 -s 192.168.0.0/16 -j DNAT --to 127.0.0.1:8000
sudo iptables -t nat -I PREROUTING -p tcp --dport 5001 -s 192.168.0.0/16 -j DNAT --to 127.0.0.1:5000
sudo iptables -t nat -I PREROUTING -p tcp --dport 8081 -s 192.168.0.0/16 -j DNAT --to 127.0.0.1:8080
```

### LIST ALL

```bash
sudo iptables -L
```

### RESET

```bash
sudo iptables -F
sudo iptables -P INPUT ACCEPT
sudo iptables -P OUTPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
```

## IPv6

Same rules but address change and ICMP rules change too:

### NAT with port redirect for custom HTTP service

Some custom HTTP services have weakness, to protect this services against port scanning i redirect some ports.
 - Launch your custom service on ::1:8000 and use it on 192.168.0.0/16:8001
 - Launch your custom service on ::1:8000 and use it on 192.168.0.0/16:5001
 - Launch your custom service on ::1:8000 and use it on 192.168.0.0/16:8081

```bash
sudo sysctl -w net.ipv6.conf.all.route_localnet=1
sudo ip6tables -t nat -I PREROUTING -p tcp --dport 8001 -s fe80::/10 -j DNAT --to ::1 --to-ports 8000
sudo ip6tables -t nat -I PREROUTING -p tcp --dport 5001 -s fe80::/10 -j DNAT --to ::1 --to-ports 5000
sudo ip6tables -t nat -I PREROUTING -p tcp --dport 8081 -s fe80::/10 -j DNAT --to ::1 --to-ports 8080
```

### Add ICMP OUTPUT rules for MAC address resolution

```bash
sudo ip6tables -I OUTPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-advertisement -j ACCEPT
sudo ip6tables -I OUTPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-solicitation -j ACCEPT
```

### Add ICMP INPUT rules for MAC address resolution

```bash
sudo ip6tables -I INPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-advertisement -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-solicitation -j ACCEPT
```

### Change ICMP call

```bash
sudo ip6tables -I OUTPUT_PROTO -p icmpv6 -j OUTPUT_ICMP
sudo ip6tables -I INPUT_PROTO -p icmpv6 -j INPUT_ICMP
```

# Other rules

## Block IP (14.15.16.17) on specific interface

```bash
iptables -A INPUT -i eth0 -s 14.15.16.17 -j DROP
```

## Block IP range (14.15.16.17/24) on specific interface

```bash
iptables -A INPUT -i eth0 -s 14.15.16.17/24 -j DROP
```

## Block port
```bash
iptables -A OUTPUT -p tcp --dport 445 -j DROP
iptables -A INPUT -p tcp --dport 445 -j DROP
```

## Block MAC address
```bash
iptables -A INPUT -m mac --mac-source 00:11:22:33:44:55 -j DROP
```

## Limit connections by host and source port
```bash
iptables -A INPUT -p tcp --syn --dport 22 -m connlimit --connlimit-above 3 -j REJECT
```

## Logs
```bash
iptables -A INPUT -i eth0 -j LOG --log-prefix "iptables logs - INPUT eth0: "
```

# Scripts

## ALL

This script is useful on your private network.
 - protect against SSH bruteforce
 - protect against FTP bruteforce
 - protect against port scanning (SYN scan, FYN scan, Xmas scan, NULL scan, UDP scan, TCP scan)
 - You can use ping command, and other computer can ping you with limit of 1 ping/seconds
 - You can use SSH, FTP, HTTP(S), MySQL, ORACLE, MongoDB, PostgreSQL, SQLServer server
 - You are protected from invalid packet or to large packet
 - You can use SSH, FTP, HTTP(S), SMTP(S), MySQL, ORACLE, MongoDB, PostgreSQL, SQLServer, IMAP(S), POP3(S), LDAP(S) client
 - You can use DHCP, DNS, SSDP, NetBIOS, LLMNR and MDNS
 - protect against UDP flooding
 - All connections on localhost are accepted
 - You can use Django Web App or python3 http.server on localhost:8000 and use it on 192.168.0.0/24:8001
 - You can use Flask on localhost:5000 and use it on 192.168.0.0/24:5001
 - You can use HTTP server on localhost:8080 and use it on 192.168.0.0/24:8088

```bash
echo "*************BEFORE*************"
echo "*************BEFORE*************"
echo "*************BEFORE*************"
echo ""
sudo iptables -L
sudo iptables -F
sudo iptables -P INPUT ACCEPT
sudo iptables -P OUTPUT ACCEPT
sudo iptables -P FORWARD ACCEPT

sudo ip6tables -L
sudo ip6tables -F
sudo ip6tables -P INPUT ACCEPT
sudo ip6tables -P OUTPUT ACCEPT
sudo ip6tables -P FORWARD ACCEPT
echo "*************CLEAR*************"
echo "*************CLEAR*************"
echo "*************CLEAR*************"
echo ""
sudo iptables -L
sudo ip6tables -L

sudo iptables -P INPUT DROP
sudo iptables -P OUTPUT DROP
sudo iptables -P FORWARD DROP
sudo ip6tables -P INPUT DROP
sudo ip6tables -P OUTPUT DROP
sudo ip6tables -P FORWARD DROP

sudo iptables -N BRUTEFORCE_FTP
sudo ip6tables -N BRUTEFORCE_FTP

sudo iptables -I BRUTEFORCE_FTP -p tcp --dport 21 -m state --state NEW -m recent --set
sudo iptables -I BRUTEFORCE_FTP -p tcp --dport 21 -m state --state NEW -m recent  --update --seconds 86400 --hitcount 25 -j DROP
sudo ip6tables -I BRUTEFORCE_FTP -p tcp --dport 21 -m state --state NEW -m recent --set
sudo ip6tables -I BRUTEFORCE_FTP -p tcp --dport 21 -m state --state NEW -m recent  --update --seconds 86400 --hitcount 25 -j DROP

sudo iptables -N BRUTEFORCE_SSH
sudo ip6tables -N BRUTEFORCE_SSH

sudo iptables -I BRUTEFORCE_SSH -p tcp --dport 22 -m state --state NEW -m recent --set
sudo iptables -I BRUTEFORCE_SSH -p tcp --dport 22 -m state --state NEW -m recent  --update --seconds 86400 --hitcount 25 -j DROP
sudo ip6tables -I BRUTEFORCE_SSH -p tcp --dport 22 -m state --state NEW -m recent --set
sudo ip6tables -I BRUTEFORCE_SSH -p tcp --dport 22 -m state --state NEW -m recent  --update --seconds 86400 --hitcount 25 -j DROP

sudo iptables -N INPUT_TCP
sudo ip6tables -N INPUT_TCP

sudo iptables -I INPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo iptables -I INPUT_TCP -p tcp ! --syn -m conntrack --ctstate NEW -j DROP
sudo iptables -I INPUT_TCP -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP
sudo iptables -I INPUT_TCP -p TCP -m multiport --dport 80,22,21,443,445,3389,8000,5000,8080 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I INPUT_TCP -p TCP -m multiport --dport 3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I INPUT_TCP -m state --state NEW -m recent --set
sudo iptables -I INPUT_TCP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP
sudo iptables -I INPUT_TCP -p tcp --dport 22 -j BRUTEFORCE_SSH
sudo iptables -I INPUT_TCP -p tcp --dport 21 -j BRUTEFORCE_FTP
sudo ip6tables -I INPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo ip6tables -I INPUT_TCP -p tcp ! --syn -m conntrack --ctstate NEW -j DROP
sudo ip6tables -I INPUT_TCP -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP
sudo ip6tables -I INPUT_TCP -p TCP -m multiport --dport 80,22,21,443,8000,5000,8080 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -I INPUT_TCP -p TCP -m multiport --dport 3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -I INPUT_TCP -m state --state NEW -m recent --set
sudo ip6tables -I INPUT_TCP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP
sudo ip6tables -I INPUT_TCP -p tcp --dport 22 -j BRUTEFORCE_SSH
sudo ip6tables -I INPUT_TCP -p tcp --dport 21 -j BRUTEFORCE_FTP

sudo iptables -N INPUT_UDP
sudo ip6tables -N INPUT_UDP

sudo iptables -I INPUT_UDP -p udp -m multiport --dport 68,53,1900,67,5355,5353,137 -j ACCEPT
sudo iptables -I INPUT_UDP -m state --state NEW -m recent --set
sudo iptables -I INPUT_UDP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP
sudo ip6tables -I INPUT_UDP -p udp -m multiport --sport 68,53,1900,67,5355,5353,137 -j ACCEPT
sudo ip6tables -I INPUT_UDP -m state --state NEW -m recent --set
sudo ip6tables -I INPUT_UDP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP

sudo iptables -N INPUT_ICMP
sudo ip6tables -N INPUT_ICMP

sudo iptables -I INPUT_ICMP -p icmp --icmp-type echo-request -m limit --limit 1/second -j ACCEPT
sudo iptables -I INPUT_ICMP -p icmp --icmp-type 11 -m limit --limit 1/second -j ACCEPT
sudo iptables -I INPUT_ICMP -p ICMP --icmp-type echo-reply -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p icmpv6 --icmpv6-type echo-request -m limit --limit 1/second -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p icmpv6 --icmpv6-type 11 -m limit --limit 1/second -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p icmpv6 --icmpv6-type echo-reply -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-advertisement -j ACCEPT
sudo ip6tables -I INPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-solicitation -j ACCEPT

sudo iptables -N INPUT_PROTO
sudo iptables -I INPUT_PROTO -p tcp -j INPUT_TCP
sudo iptables -I INPUT_PROTO -p udp -j INPUT_UDP
sudo iptables -I INPUT_PROTO -p icmp -j INPUT_ICMP
sudo ip6tables -N INPUT_PROTO
sudo ip6tables -I INPUT_PROTO -p tcp -j INPUT_TCP
sudo ip6tables -I INPUT_PROTO -p udp -j INPUT_UDP
sudo ip6tables -I INPUT_PROTO -p icmpv6 -j INPUT_ICMP

sudo iptables -N OUTPUT_ICMP
sudo iptables -I OUTPUT_ICMP -p ICMP --icmp-type echo-reply -m limit --limit 1/second -j ACCEPT
sudo iptables -I OUTPUT_ICMP -p ICMP --icmp-type echo-request -j ACCEPT
sudo ip6tables -N OUTPUT_ICMP
sudo ip6tables -I OUTPUT_ICMP -p icmpv6 --icmpv6-type echo-reply -m limit --limit 1/second -j ACCEPT
sudo ip6tables -I OUTPUT_ICMP -p icmpv6 --icmpv6-type echo-request -j ACCEPT
sudo ip6tables -I OUTPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-advertisement -j ACCEPT
sudo ip6tables -I OUTPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-solicitation -j ACCEPT

sudo iptables -N OUTPUT_TCP
sudo iptables -I OUTPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo iptables -I OUTPUT_TCP -p TCP -m multiport --dport 80,443,22,21,25,587,465,143,993,110,995,389,636 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I OUTPUT_TCP -p TCP -m multiport --dport 445,3389,8001,5001,5000,8080,8088,8000 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -I OUTPUT_TCP -p TCP -m multiport --dport 3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -N OUTPUT_TCP
sudo ip6tables -I OUTPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo ip6tables -I OUTPUT_TCP -p TCP -m multiport --dport 80,443,22,21,25,587,465,143,993,110,995,389,636 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -I OUTPUT_TCP -p TCP -m multiport --dport 8001,5001,5000,8080,8088,8000 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -I OUTPUT_TCP -p TCP -m multiport --dport 3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268 --syn -m conntrack --ctstate NEW -j ACCEPT

sudo iptables -N OUTPUT_UDP
sudo iptables -I OUTPUT_UDP -p udp -m multiport --dport 68,53,1900,67,5355,5353,137 -j ACCEPT
sudo ip6tables -N OUTPUT_UDP
sudo ip6tables -I OUTPUT_UDP -p udp -m multiport --dport 68,53,1900,67,5355,5353,137 -j ACCEPT

sudo iptables -N OUTPUT_PROTO
sudo iptables -I OUTPUT_PROTO -p tcp -j OUTPUT_TCP
sudo iptables -I OUTPUT_PROTO -p udp -j OUTPUT_UDP
sudo iptables -I OUTPUT_PROTO -p icmp -j OUTPUT_ICMP
sudo ip6tables -N OUTPUT_PROTO
sudo ip6tables -I OUTPUT_PROTO -p tcp -j OUTPUT_TCP
sudo ip6tables -I OUTPUT_PROTO -p udp -j OUTPUT_UDP
sudo ip6tables -I OUTPUT_PROTO -p icmpv6 -j OUTPUT_ICMP

sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A OUTPUT -o lo -j ACCEPT
sudo ip6tables -A INPUT -i lo -j ACCEPT
sudo ip6tables -A OUTPUT -o lo -j ACCEPT

sudo iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
sudo iptables -A INPUT -j INPUT_PROTO
sudo iptables -A OUTPUT -j OUTPUT_PROTO
sudo ip6tables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
sudo ip6tables -A INPUT -j INPUT_PROTO
sudo ip6tables -A OUTPUT -j OUTPUT_PROTO

sudo sysctl -w net.ipv4.conf.all.route_localnet=1
sudo iptables -t nat -I PREROUTING -p tcp --dport 8001 -s 192.168.0.0/16 -j DNAT --to 127.0.0.1:8000
sudo iptables -t nat -I PREROUTING -p tcp --dport 5001 -s 192.168.0.0/16 -j DNAT --to 127.0.0.1:5000
sudo iptables -t nat -I PREROUTING -p tcp --dport 8081 -s 192.168.0.0/16 -j DNAT --to 127.0.0.1:8080
sudo ip6tables -t nat -I PREROUTING -p tcp --dport 8001 -s fe80::/10 -j DNAT --to [::1]:8000
sudo ip6tables -t nat -I PREROUTING -p tcp --dport 5001 -s fe80::/10 -j DNAT --to [::1]:5000
sudo ip6tables -t nat -I PREROUTING -p tcp --dport 8081 -s fe80::/10 -j DNAT --to [::1]:8080

echo "*************AFTER*************"
echo "*************AFTER*************"
echo "*************AFTER*************"
echo ""
sudo iptables -L
sudo ip6tables -L
```

## Minimum

This script is useful for untrusted network.
 - ICMP (ping and other) are dropped (block host detection with ping)
 - protect against port scanning (SYN scan, FYN scan, Xmas scan, NULL scan, UDP scan, TCP scan)
 - You are protected from invalid packet or to large packet
 - You can use SSH, FTP, HTTP(S), SMTP(S), IMAP(S) and POP3(S) client
 - You can use DNS and DHCP
 - protect against UDP flooding
 - All connections on localhost are accepted

```bash
echo "*************BEFORE*************"
echo "*************BEFORE*************"
echo "*************BEFORE*************"
echo ""
sudo iptables -L
sudo iptables -F
sudo iptables -P INPUT ACCEPT
sudo iptables -P OUTPUT ACCEPT
sudo iptables -P FORWARD ACCEPT

sudo ip6tables -L
sudo ip6tables -F
sudo ip6tables -P INPUT ACCEPT
sudo ip6tables -P OUTPUT ACCEPT
sudo ip6tables -P FORWARD ACCEPT
echo "*************CLEAR*************"
echo "*************CLEAR*************"
echo "*************CLEAR*************"
echo ""
sudo iptables -L
sudo ip6tables -L

sudo iptables -P INPUT DROP
sudo iptables -P OUTPUT DROP
sudo iptables -P FORWARD DROP
sudo ip6tables -P INPUT DROP
sudo ip6tables -P OUTPUT DROP
sudo ip6tables -P FORWARD DROP

sudo iptables -N INPUT_TCP
sudo ip6tables -N INPUT_TCP

sudo iptables -I INPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo iptables -I INPUT_TCP -p tcp ! --syn -m conntrack --ctstate NEW -j DROP
sudo iptables -I INPUT_TCP -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP
sudo iptables -I INPUT_TCP -m state --state NEW -m recent --set
sudo iptables -I INPUT_TCP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP
sudo ip6tables -I INPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo ip6tables -I INPUT_TCP -p tcp ! --syn -m conntrack --ctstate NEW -j DROP
sudo ip6tables -I INPUT_TCP -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP
sudo ip6tables -I INPUT_TCP -m state --state NEW -m recent --set
sudo ip6tables -I INPUT_TCP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP

sudo iptables -N INPUT_UDP
sudo ip6tables -N INPUT_UDP

sudo iptables -I INPUT_UDP -p udp -m multiport --sport 68,53 -j ACCEPT
sudo iptables -I INPUT_UDP -m state --state NEW -m recent --set
sudo iptables -I INPUT_UDP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP
sudo ip6tables -I INPUT_UDP -p udp -m multiport --sport 68,53 -j ACCEPT
sudo ip6tables -I INPUT_UDP -m state --state NEW -m recent --set
sudo ip6tables -I INPUT_UDP -m state --state NEW -m recent --update --seconds 20 --hitcount 15 -j DROP

sudo ip6tables -N INPUT_ICMP

ip6tables -I INPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-advertisement -j ACCEPT
ip6tables -I INPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-solicitation -j ACCEPT

sudo iptables -N INPUT_PROTO
sudo iptables -I INPUT_PROTO -p tcp -j INPUT_TCP
sudo iptables -I INPUT_PROTO -p udp -j INPUT_UDP
sudo ip6tables -N INPUT_PROTO
sudo ip6tables -I INPUT_PROTO -p tcp -j INPUT_TCP
sudo ip6tables -I INPUT_PROTO -p udp -j INPUT_UDP
sudo ip6tables -I INPUT_PROTO -p icmpv6 -j INPUT_ICMP

sudo ip6tables -N OUTPUT_ICMP
sudo ip6tables -I OUTPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-advertisement -j ACCEPT
sudo ip6tables -I OUTPUT_ICMP -p ipv6-icmp --icmpv6-type neighbor-solicitation -j ACCEPT

sudo iptables -N OUTPUT_TCP
sudo iptables -I OUTPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo iptables -I OUTPUT_TCP -p TCP -m multiport --dport 80,443,22,21,25,587,465,143,993,110,995 --syn -m conntrack --ctstate NEW -j ACCEPT
sudo ip6tables -N OUTPUT_TCP
sudo ip6tables -I OUTPUT_TCP -m conntrack --ctstate ESTABLISHED -j ACCEPT
sudo ip6tables -I OUTPUT_TCP -p TCP -m multiport --dport 80,443,22,21,25,587,465,143,993,110,995 --syn -m conntrack --ctstate NEW -j ACCEPT

sudo iptables -N OUTPUT_UDP
sudo iptables -I OUTPUT_UDP -p udp -m multiport --dport 68,53 -j ACCEPT
sudo ip6tables -N OUTPUT_UDP
sudo ip6tables -I OUTPUT_UDP -p udp -m multiport --dport 68,53 -j ACCEPT

sudo iptables -N OUTPUT_PROTO
sudo iptables -I OUTPUT_PROTO -p tcp -j OUTPUT_TCP
sudo iptables -I OUTPUT_PROTO -p udp -j OUTPUT_UDP
sudo ip6tables -N OUTPUT_PROTO
sudo ip6tables -I OUTPUT_PROTO -p tcp -j OUTPUT_TCP
sudo ip6tables -I OUTPUT_PROTO -p udp -j OUTPUT_UDP
sudo ip6tables -I OUTPUT_PROTO -p icmpv6 -j OUTPUT_ICMP

sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A OUTPUT -o lo -j ACCEPT
sudo ip6tables -A INPUT -i lo -j ACCEPT
sudo ip6tables -A OUTPUT -o lo -j ACCEPT

sudo iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
sudo iptables -A INPUT -j INPUT_PROTO
sudo iptables -A OUTPUT -j OUTPUT_PROTO
sudo ip6tables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
sudo ip6tables -A INPUT -j INPUT_PROTO
sudo ip6tables -A OUTPUT -j OUTPUT_PROTO

sysctl -w net.ipv4.conf.all.route_localnet=0

echo "*************AFTER*************"
echo "*************AFTER*************"
echo "*************AFTER*************"
echo ""
sudo iptables -L
sudo ip6tables -L
```
