# CustomIPTABLES_rules

Some scripts for Linux firewall on trusted network or untrusted network to protect your computer.

All rules and chains are explain in `iptables.md`, `trusted_network.sh` and `untrusted_network.sh` are explain too.

You can add alias for firewall configuration like this:

```bash
git clone https://gitlab.com/MauriceLambert/customiptables_rules
cd customiptables_rules
sudo mkdir /etc/firewall
sudo mv trusted_network.sh /etc/firewall/trust.sh
sudo mv untrusted_network.sh /etc/firewall/untrust.sh
nano ~/.bash_aliases
```

in nano editor write this lines:

```bash
alias fw-trust='sudo bash /etc/firewall/trust.sh'
alias fw-untrust='sudo bash /etc/firewall/untrust.sh'
alias fw-port-output-tcp='sudo iptables -I OUTPUT_TCP_PORTS -p TCP --syn -m conntrack --ctstate NEW -j ACCEPT --dport '
alias fw-port-output-udp='sudo iptables -I OUTPUT_UDP_PORTS -p UDP -j ACCEPT --dport '
alias fw-port-input-tcp='sudo iptables -I INPUT_TCP_PORTS -p TCP --syn -m conntrack --ctstate NEW -j ACCEPT --dport '
alias fw-port-input-udp='sudo iptables -I INPUT_UDP_PORTS -p UDP -j ACCEPT --sport '
alias fw-blacklist-input-ip='sudo iptables -I INPUT -j DROP -s '
alias fw-blacklist-output-ip='sudo iptables -I OUTPUT -j DROP -s '
alias fw6-port-output-tcp='sudo ip6tables -I OUTPUT_TCP_PORTS -p TCP --syn -m conntrack --ctstate NEW -j ACCEPT --dport '
alias fw6-port-output-udp='sudo ip6tables -I OUTPUT_UDP_PORTS -p UDP -j ACCEPT --dport '
alias fw6-port-input-tcp='sudo ip6tables -I INPUT_TCP_PORTS -p TCP --syn -m conntrack --ctstate NEW -j ACCEPT --dport '
alias fw6-port-input-udp='sudo ip6tables -I INPUT_UDP_PORTS -p UDP -j ACCEPT --sport '
alias fw6-blacklist-input-ip='sudo ip6tables -I INPUT -j DROP -s '
alias fw6-blacklist-output-ip='sudo ip6tables -I OUTPUT -j DROP -s '
```

Now you can open a new terminal and write `fw-trust` to start new firewall configuration.

For persistent firewall configuration:

```bash
sudo touch /etc/rc.local
sudo chmod 100 /etc/rc.local
sudo echo "#\!/bin/bash" > /etc/rc.local
sudo echo "/bin/bash /etc/firewall/trust.sh > /dev/null" >> /etc/rc.local
```
