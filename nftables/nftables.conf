#!/usr/sbin/nft -f

flush ruleset

table inet filter {                                                                # ipv4 && ipv6
    set untrusted_tcp_ports {                                                      # TCP ports for untrusted network
        typeof ip port
        # type tcp
        elements = {80,443,22,21,25,587,465,143,993,110,995}
    }

    set trusted_tcp_ports1 {                                                      # TCP ports for trusted network
        typeof ip port
        # type tcp
        elements = {389,636,445,3389,8001,5001,5000,8080,8088,8000}
    }

    set trusted_tcp_ports2 {                                                      # TCP ports for trusted network
        typeof ip port
        # type tcp
        elements = {3306,3307,1433,1434,27017,27018,27019,5433,5434,1521,1526,3269,3268}
    }

    set untrusted_udp_ports {                                                     # UDP ports for untrusted network
        typeof ip port
        # type udp
        elements = {53,68}
    }

    set trusted_udp_ports {                                                       # UDP ports for trusted network
        typeof ip port
        # type udp
        elements = {1900,67,5355,5353,137}
    }

    set bruteforce_ssh {                                                          # Bruteforce SSH blacklist IPv4
        type ipv4_addr
        flags dynamic, timeout
        timeout 10m
    }

    set bruteforce_ftp {                                                          # Bruteforce FTP blacklist IPv4
        type ipv4_addr
        flags dynamic, timeout
        timeout 10m
    }

    set portscan {                                                                # Port scan blacklist IPv4
        type ipv4_addr
        flags dynamic, timeout
        timeout 1m
    }

    set bruteforce_ssh6 {                                                         # Bruteforce SSH blacklist IPv6
        type ipv6_addr
        flags dynamic, timeout
        timeout 10m
    }

    set bruteforce_ftp6 {                                                         # Bruteforce FTP blacklist IPv6
        type ipv6_addr
        flags dynamic, timeout
        timeout 10m
    }

    set portscan6 {                                                               # Port scan blacklist IPv6
        type ipv6_addr
        flags dynamic, timeout
        timeout 1m
    }

    chain input_tcp {                                                             # Verdict INPUT new TCP packets
        tcp flags != syn / fin,syn,rst,ack ct state new counter drop              # Non only SYN new TCP packets
        tcp ct state new tcp option maxseg size != 536-65535 counter drop         # Block to short and to long TCP packets

        tcp dport 22 ct state new, untracked limit rate over 3/minute add @bruteforce_ssh { ip saddr }
        ip saddr @bruteforce_ssh drop                                             # Block SSH bruteforce
        tcp dport 21 ct state new, untracked limit rate over 3/minute add @bruteforce_ftp { ip saddr }
        ip saddr @bruteforce_ftp drop                                             # Block FTP bruteforce
        tcp ct state new, untracked limit rate over 1/second add @portscan { ip saddr }
        ip saddr @portscan drop                                                   # Block SYN/TCP port scan

        tcp dport 22 ct state new, untracked limit rate over 3/minute add @bruteforce_ssh6 { ip6 saddr }
        ip6 saddr @bruteforce_ssh drop                                             # Block SSH bruteforce
        tcp dport 21 ct state new, untracked limit rate over 3/minute add @bruteforce_ftp6 { ip6 saddr }
        ip6 saddr @bruteforce_ftp drop                                             # Block FTP bruteforce
        tcp ct state new, untracked limit rate over 1/second add @portscan6 { ip6 saddr }
        ip6 saddr @portscan drop                                                   # Block SYN/TCP port scan

        tcp dport @untrusted_tcp_ports tcp flags syn / fin,syn,rst,ack ct state new counter accept
        tcp dport @trusted_tcp_ports1 tcp flags syn / fin,syn,rst,ack ct state new counter accept
        tcp dport @trusted_tcp_ports2 tcp flags syn / fin,syn,rst,ack ct state new counter accept
                                                                                  # Accept new TCP connections for allowed ports
    }

    chain input_udp {                                                             # Verdict INPUT new UDP packets
        udp dport @untrusted_udp_ports ct state new counter accept                # Accept new UDP connections for allowed ports
        udp dport @trusted_udp_ports ct state new counter accept
    }

    chain input_icmp {                                                            # Verdict INPUT ICMP packets
        icmp type { echo-request, time-exceeded } limit rate 1/second burst 5 packets counter accept
                                                                                  # Accept ICMP ping request limited 1/second
        icmp type echo-reply counter accept                                       # Accept ICMP ping reply

        meta l4proto ipv6-icmp icmpv6 type { nd-neighbor-advert, nd-neighbor-solicit } counter accept
    }

    chain input {
        type filter hook input priority 0;
        policy drop;                                                              # Drop by default

        iifname "lo" counter accept;                                              # Accept localhost

        ct state established,related counter accept;                              # Accept established connections

        ip protocol udp counter jump input_udp
        ip protocol tcp counter jump input_tcp
        ip protocol icmp counter jump input_icmp
    }

    chain output_tcp {
        tcp flags != syn / fin,syn,rst,ack ct state new counter drop              # Non only SYN new TCP packets
        tcp ct state new tcp option maxseg size != 536-65535 counter drop         # Block to short and to long TCP packets

        tcp dport @untrusted_tcp_ports tcp flags syn / fin,syn,rst,ack ct state new counter accept
        tcp dport @trusted_tcp_ports1 tcp flags syn / fin,syn,rst,ack ct state new counter accept
        tcp dport @trusted_tcp_ports2 tcp flags syn / fin,syn,rst,ack ct state new counter accept
    }

    chain output_udp {                                                            # Verdict INPUT new UDP packets
        udp dport @untrusted_udp_ports ct state new counter accept                # Accept new UDP connections for allowed ports
        udp dport @trusted_udp_ports ct state new counter accept
    }

    chain output_icmp {                                                           # Verdict OUTPUT ICMP packets
        icmp type echo-reply limit rate 1/second burst 5 packets counter accept
                                                                                  # Accept ICMP ping request limited 1/second
        icmp type echo-request counter accept                                     # Accept ICMP ping reply

        meta l4proto ipv6-icmp icmpv6 type nd-neighbor-advert counter accept
        meta l4proto ipv6-icmp icmpv6 type nd-neighbor-solicit counter accept
    }

    chain output {
        type filter hook input priority 0;
        policy drop;                                                              # Drop by default

        oifname "lo" counter accept;                                              # Accept localhost

        ip protocol udp counter jump output_udp
        ip protocol tcp counter jump output_tcp
        ip protocol icmp counter jump output_icmp
    }

    chain forward {}
}

table nat filter {                                                                # ipv4 && ipv6
    map nat_services {
        type inet_service : ipv4_addr . inet_service
        elements = {
            8001: 127.0.0.1 . 8000,
            5001: 127.0.0.1 . 5000,
            8081: 127.0.0.1 . 8080,
        }
    }

    map nat_services6 {
        type inet_service : ipv6_addr . inet_service
        elements = {
            8001: [::1] . 8000,
            5001: [::1] . 5000,
            8081: [::1] . 8080,
        }
    }

    set ip_local {
        type ipv4_addr
        flags interval
        elements = {10.0.0.0/8, 172.16.0.0/16, 192.168.0.0/24}
    }

    set ip6_local {
        type ipv4_addr
        flags interval
        elements = {fe80::/10}
    }

    chain prerouting {
        ct state invalid counter drop
        ip saddr @ip_local dnat to tcp dport map @nat_services
        ip saddr @ip6_local dnat to tcp dport map @nat_services6
    }

    chain postrouting {}
    chain ingress {}
}
